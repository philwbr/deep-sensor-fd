import scipy.io as sio
import csv

# 0. Import only txt files based on mat file exports


with open('/home/phil/Code+/data/track-data/dataleo.csv') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    line_count = 0
    for row in csv_reader:
        if line_count == 0:
            print(f'Column names are {", ".join(row)}')
            line_count += 1
        else:
            print(f'\t{row[0]} works in the {row[1]} department, and was born in {row[2]}.')
            line_count += 1
    print(f'Processed {line_count} lines.')


# 1. Impprt Matfiles
mat = sio.loadmat ('/home/phil/Code+/data/track-data/MO_Q1_36.mat')

#print (mat)


# 2. Divide into different channels



class DataLeoMax:
    def __init__(self, config):
        self.config = config
        # load data here
        self.input = np.ones((500, 784))
        self.y = np.ones((500, 10))

    def next_batch(self, batch_size):
        idx = np.random.choice(500, batch_size)
        yield self.input[idx], self.y[idx]
